package com.example.selabeautyadmin.ui.profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import com.example.selabeautyadmin.APIRequestData;
import com.example.selabeautyadmin.LoginActivity;
import com.example.selabeautyadmin.ResponseModel;
import com.example.selabeautyadmin.RetroServer;
import com.example.selabeautyadmin.SPHandler;
import com.example.selabeautyadmin.databinding.FragmentNotificationsBinding;
import com.example.selabeautyadmin.ui.staff.DashboardViewModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationsFragment extends Fragment {
    SPHandler sp = new SPHandler();
    private DashboardViewModel listProductData;
    private DashboardViewModel listData;

    private int pid;
    private int page;
    private String pname;
    private String puname;
    private String pemail, pcreate;

    TextView tvid, tvname, tvuname, tvemail,tvage, tvcreate, tvrole;
    Button btnedit, btnchange, btnout;

    private FragmentNotificationsBinding binding;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        DashboardViewModel notificationsViewModel =
                new ViewModelProvider(this).get(DashboardViewModel.class);

        binding = FragmentNotificationsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        tvname = binding.name;
        tvuname = binding.uname;
        tvemail = binding.email;
        tvage = binding.age;
        tvcreate = binding.create;
        tvrole=binding.role;
        btnedit = binding.btnEdit;
        btnchange = binding.btnChange;
        btnout = binding.btnLogout;
        tvrole.setText(sp.getRole(getActivity()));
        getProfile();

        btnedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData(pid);
            }
        });

        btnchange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChangePassFragment cpf = new ChangePassFragment();
                Bundle args = new Bundle();
                args.putInt("id", pid);
                cpf.setArguments(args);
                replaceFragment(cpf,"change_pass");
            }
        });

        btnout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sp.removeLog(getActivity());
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
            }
        });

        return root;
    }
    @Override
    public void onResume() {
        super.onResume();
        getProfile();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private void getProfile(){
        APIRequestData ardData = RetroServer.konekRetrofit().create(APIRequestData.class);
        Call<ResponseModel> profile = ardData.ardProfile(sp.getToken(getActivity()));

        profile.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                if(response.isSuccessful()) {
                    listProductData = response.body().getUserbyid();

                    pid = listProductData.getUserid();
                    page = listProductData.getAge();
                    pname = listProductData.getName();
                    pemail = listProductData.getEmail();
                    puname = listProductData.getUsername();
                    pcreate = listProductData.getCreate_at();


                    tvname.setText(pname);
                    tvuname.setText(puname);
                    tvemail.setText(pemail);
                    tvage.setText(String.valueOf(page));
                    tvcreate.setText("join at " + pcreate);
                    //Toast.makeText(getActivity(), pcreate, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(getActivity(), "Gagal Menghubungi Server : " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getData(int p){
        APIRequestData ardData = RetroServer.konekRetrofit().create(APIRequestData.class);
        Call<ResponseModel> getProduct = ardData.ardStaffId(sp.getToken(getActivity()), p);

        getProduct.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                listData = response.body().getUserbyid();

                int varId = listData.getUserid();
                int varAge = listData.getAge();
                String varNama = listData.getName();
                String varEmail = listData.getEmail();
                String varUsername = listData.getUsername();
                String varCreate = listData.getCreate_at();

                EditProfileFragment esf = new EditProfileFragment();
                Bundle args = new Bundle();
                args.putInt("id", varId);
                args.putInt("age", varAge);
                args.putString("name", varNama);
                args.putString("email",varEmail);
                args.putString("uname", varUsername);
                esf.setArguments(args);
                replaceFragment(esf,"update_profile");
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(getActivity(), "Gagal Menghubungi Server : " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void replaceFragment(Fragment fragment,String targettag){
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(((ViewGroup)getView().getParent()).getId(),fragment,targettag);
        fragmentTransaction.addToBackStack("profile_fragment").commit();
    }
}