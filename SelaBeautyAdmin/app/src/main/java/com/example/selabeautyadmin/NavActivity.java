package com.example.selabeautyadmin;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.selabeautyadmin.databinding.ActivityAdminNavBinding;
import com.example.selabeautyadmin.ui.product.HomeFragment;
import com.example.selabeautyadmin.ui.profile.NotificationsFragment;
import com.example.selabeautyadmin.ui.staff.DashboardFragment;

public class NavActivity extends AppCompatActivity {

    private ActivityAdminNavBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAdminNavBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        replaceFragment(new HomeFragment(),"product_fragment");
        binding.navView.setOnItemSelectedListener(item ->{
            switch (item.getItemId()){
                case R.id.navigation_home:
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    replaceFragment(new HomeFragment(),"product_fragment");
                    break;
                case R.id.navigation_dashboard:
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    replaceFragment(new DashboardFragment(),"staff_fragment");
                    break;
                case R.id.navigation_notifications:
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    replaceFragment(new NotificationsFragment(),"profile_fragment");
                    break;
            }
            return true;
        });
    }
    private void replaceFragment(Fragment fragment,String tag){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout,fragment,tag);
        fragmentTransaction.commit();
    }

}