package com.example.selabeautyadmin.ui.product;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.selabeautyadmin.APIRequestData;
import com.example.selabeautyadmin.R;
import com.example.selabeautyadmin.ResponseModel;
import com.example.selabeautyadmin.RetroServer;
import com.example.selabeautyadmin.SPHandler;
import com.example.selabeautyadmin.databinding.FragmentHomeBinding;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment implements ProductAdapter.EventListener, AdapterView.OnItemSelectedListener {
    private RecyclerView review;
    private SwipeRefreshLayout srlData;
    private List<HomeViewModel> listData = new ArrayList<>();
    private HomeViewModel listProductData;
    private ProgressBar pbData;
    private FloatingActionButton fabTambah, fabfilter;
    private RecyclerView.LayoutManager lmData;
    private RecyclerView.Adapter adapter;
    private boolean doubleBackToExitPressedOnce = false;
    private FragmentHomeBinding binding;
    int acat;
    SPHandler sp = new SPHandler();
    Spinner spin;
    String[] category = { "Pilih Kategori","Bodycare", "Haircare",
            "Makeup", "Skincare"};

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        HomeViewModel homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        review = binding.rvData;
        srlData = binding.srlData;
        pbData = binding.pbData;
        fabTambah = binding.addicon;
        fabfilter = binding.filtericon;
        spin = binding.pCat;
        spin.setVisibility(View.INVISIBLE);
        ArrayAdapter ad = new ArrayAdapter(getActivity(), R.layout.category_value, category);
        ad.setDropDownViewResource(
                R.layout.category_value);
        spin.setAdapter(ad);
        spin.setSelection(0);
        lmData = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        review.setLayoutManager(lmData);
        retrieveData();

        srlData.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                srlData.setRefreshing(true);
                retrieveData();
                srlData.setRefreshing(false);
            }
        });
        fabTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddProductFragment nextFrag= new AddProductFragment();
                replaceFragment(nextFrag,"add_product");
                fabTambah.setVisibility(View.INVISIBLE);

            }
        });
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {retrieveData();}
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        fabfilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spin.setVisibility(View.VISIBLE);
            }
        });

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        retrieveData();
    }

    public void retrieveData(){
        pbData.setVisibility(View.VISIBLE);
        APIRequestData ardData = RetroServer.konekRetrofit().create(APIRequestData.class);
        Call<ResponseModel> showProduct = ardData.ardProductData(sp.getToken(getActivity()),spin.getSelectedItemPosition());
        showProduct.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                if(response.isSuccessful()) {
                    listData = response.body().getProduct();
                    adapter = new ProductAdapter(getActivity(), listData, HomeFragment.this);
                    review.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
                pbData.setVisibility(View.INVISIBLE);
                spin.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

                pbData.setVisibility(View.INVISIBLE);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }



    @Override
    public void onEvent(int  d) {
        getData(d);
    }

    private void getData(int p){
        APIRequestData ardData = RetroServer.konekRetrofit().create(APIRequestData.class);
        Call<ResponseModel> getProduct = ardData.ardProductDataId(sp.getToken(getActivity()), p);
        getProduct.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                listProductData = response.body().getProductbyid();
                int varIdProduct = listProductData.getProductid();
                String varNamaProduct = listProductData.getName();
                int varHargaProduct = listProductData.getPrice();
                int varCatProduct = listProductData.getCat_id();
                int varStock =listProductData.getStock();
                String varShortDesk = listProductData.getShort_desc();
                String varBrand = listProductData.getBrand();
                String varImg = listProductData.getImg();
                Drawable drawimg = getImage(varImg);
                String varDesc = listProductData.getDesc();
                DetailsProductFragment dpf = new DetailsProductFragment();
                Bundle args = new Bundle();
                args.putInt("id", varIdProduct);
                args.putInt("price", varHargaProduct);
                args.putInt("stock", varStock);
                args.putInt("cat", varCatProduct);
                args.putString("name", varNamaProduct);
                args.putString("brand",varBrand);
                args.putString("short", varShortDesk);
                args.putString("desc", varDesc);
                args.putString("img", varImg);
                dpf.setArguments(args);
                replaceFragment(dpf,"details_product");
                fabTambah.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(getActivity(), "Gagal Menghubungi Server : " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    public Drawable getImage(String url){
        try {
            InputStream is = (InputStream) new URL(url).getContent();
            Drawable d = Drawable.createFromStream(is, "eye_cream.jpg");
            return d;
        } catch (Exception e) {
            return null;
        }
    }

    private void replaceFragment(Fragment fragment,String targettag){
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(((ViewGroup)getView().getParent()).getId(),fragment,targettag);
        fragmentTransaction.addToBackStack("product_fragment").commit();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        retrieveData();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        retrieveData();
    }

//
}
