package com.example.selabeautyadmin.ui.product;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.selabeautyadmin.R;
import com.example.selabeautyadmin.SPHandler;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class ProductAdapter  extends RecyclerView.Adapter<ProductAdapter.HolderData>{
    private Context ctx;
    private List<HomeViewModel> listData;
    private HomeViewModel listProductData;
    private HomeFragment frag;
    private int pId;
    InputStream is;
    Bitmap bmImg = null;
    SPHandler sp = new SPHandler();
    EventListener listener;
    String pd;

    List<Integer> in = new ArrayList<>();

    public List<Integer> addNo (List<Integer> i, int n){
        int a;
        for(a=1; a<n; ++a){
            i.add(a);
        }
        return i;
    }


    public interface EventListener {
        void onEvent(int data);
    }
    public ProductAdapter(Context ctx, List<HomeViewModel> listData, EventListener l) {
        this.listener = l;
        this.ctx = ctx;
        this.listData = listData;
    }

    @NonNull
    @Override
    public HolderData onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.product, parent, false);
        HolderData holder = new HolderData(layout);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull HolderData holder, int position) {
        HomeViewModel pm = listData.get(position);
        List<Integer> iter = addNo(in, getItemCount()+1);
        holder.id.setText(String.valueOf(pm.getProductid()));
        holder.Nama.setText(pm.getBrand()+" "+pm.getName());
        holder.Harga.setText(" Rp "+String.valueOf(pm.getPrice()));
        holder.No.setText(String.valueOf(iter.get(position)));    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class HolderData extends RecyclerView.ViewHolder {
        TextView id, Nama, Harga, No;


        public HolderData(@NonNull View itemView) {
            super(itemView);

            id = itemView.findViewById(R.id.productid);
            No = itemView.findViewById(R.id.no);
            Nama = itemView.findViewById(R.id.nama);
            Harga = itemView.findViewById(R.id.harga);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onEvent(Integer.parseInt(id.getText().toString()));

                }
            });
//
        }
    }

//
}
