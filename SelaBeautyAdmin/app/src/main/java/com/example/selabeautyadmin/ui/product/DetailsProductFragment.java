package com.example.selabeautyadmin.ui.product;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.selabeautyadmin.APIRequestData;
import com.example.selabeautyadmin.R;
import com.example.selabeautyadmin.ResponseModel;
import com.example.selabeautyadmin.RetroServer;
import com.example.selabeautyadmin.SPHandler;
import com.example.selabeautyadmin.databinding.FragmentDetailsProductBinding;

import java.io.InputStream;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DetailsProductFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailsProductFragment extends Fragment {
    FragmentDetailsProductBinding binding;
    private Drawable Img;
    private HomeViewModel listData;
    ProgressDialog p;
    SPHandler sp = new SPHandler();
    ExecutorService executor = Executors.newSingleThreadExecutor();
    Handler handler = new Handler(Looper.getMainLooper());
    Drawable dImg = null;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String id = "id";
    private static final String price = "price";
    private static final String stock = "stock";
    private static final String cat = "cat";
    private static final String name = "name";
    private static final String shortdesc = "short";
    private static final String desc = "desc";
    private static final String img = "img";
    private static final String brand = "brand";

    // TODO: Rename and change types of parameters
    private int pid, pprice, pstock, pcat;
    private String pname, pbrand, pshort, pdesc,pimg;

    ImageView imageView;
    TextView tvid, tvname, tvshort, tvcat,tvstock, tvprice,tvdesc;
    Button btnedit, btndelete;

    public DetailsProductFragment() {

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment DetailsProductFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetailsProductFragment newInstance(int varIdProduct, int varHargaProduct, int varStock, int varCatProduct, String varNamaProduct, String varBrand, String varShortDesk, String varDesc, String varImg) {
        DetailsProductFragment fragment = new DetailsProductFragment();
        Bundle args = new Bundle();
        args.putInt("id", varIdProduct);
        args.putInt("price", varHargaProduct);
        args.putInt("stock", varStock);
        args.putInt("cat", varCatProduct);
        args.putString("name", varNamaProduct);
        args.putString("brand",varBrand);
        args.putString("short", varShortDesk);
        args.putString("desc", varDesc);
        args.putString("img", varImg);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            pid = getArguments().getInt(id);
            pprice = getArguments().getInt(price);
            pstock = getArguments().getInt(stock);
            pcat = getArguments().getInt(cat);
            pname = getArguments().getString(name);
            pbrand = getArguments().getString(brand);
            pshort = getArguments().getString(shortdesc);
            pdesc = getArguments().getString(desc);
            pimg = getArguments().getString(img);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentDetailsProductBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        tvname = binding.nama;
        tvcat = binding.cat;
        tvshort = binding.shortdesk;
        tvprice = binding.harga;
        tvstock = binding.stok;
        tvdesc = binding.desk;
        imageView = binding.imgView;
        btndelete = binding.delete;
        btnedit = binding.edit;
        doMyTask(pimg);

        btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder dialogPesan = new AlertDialog.Builder(getActivity());
                dialogPesan.setMessage("Are you sure you want to delete this product?");
                dialogPesan.setTitle("Alert");
                dialogPesan.setIcon(R.drawable.sela_beauty_logo);
                dialogPesan.setCancelable(true);
                dialogPesan.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        deleteData(pid);
                        dialogInterface.dismiss();
                        Handler hand = new Handler();
                        hand.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                removeFragment();
                            }
                        }, 1000);
                    }});

                dialogPesan.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }});
                dialogPesan.show();
            }});

        btnedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData(pid);
            }
        });

        return root;
    }

    public Drawable getImage(String url){
        try {
            InputStream is = (InputStream) new URL(url).getContent();
            Drawable d = Drawable.createFromStream(is, "src name");
            return d;
        } catch (Exception e) {
            return null;
        }
    }
    private void doMyTask(String url){
        p = new ProgressDialog(getActivity());
        p.setMessage("Getting data...");
        p.setIndeterminate(false);
        p.setCancelable(false);
        p.show();
        executor.execute(() -> {
            dImg = getImage(url);
            handler.post(() -> {
                if(imageView!=null) {
                    p.hide();
                    tvname.setText(pbrand+" "+pname);
                    if(pcat==1){
                        tvcat.setText("Bodycare");
                    }else if(pcat==2){
                        tvcat.setText("Haircare");
                    }else if(pcat==3){
                        tvcat.setText("Makeup");
                    }else if(pcat==4){
                        tvcat.setText("Skincare");
                    }

                    tvshort.setText(pshort);
                    tvprice.setText("Rp "+pprice);
                    tvstock.setText(String.valueOf(pstock));
                    tvdesc.setText(pdesc);
                    imageView.setImageDrawable(dImg);
                }else {
                    p.hide();
                    Toast.makeText(getActivity(), "Gagal mengambil data", Toast.LENGTH_SHORT).show();
                }
            });
        });
    }

    private void deleteData(int p){
        APIRequestData ardData = RetroServer.konekRetrofit().create(APIRequestData.class);
        Call<ResponseModel> hapusData = ardData.ardDeleteProduct(sp.getToken(getActivity()), p);

        hapusData.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                String pesan = response.body().getMessage();

                Toast.makeText(getActivity(), "Pesan : "+pesan, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(getActivity(), "Gagal Menghubungi Server : " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getData(int p){
        APIRequestData ardData = RetroServer.konekRetrofit().create(APIRequestData.class);
        Call<ResponseModel> getProduct = ardData.ardProductDataId(sp.getToken(getActivity()), p);

        getProduct.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                listData = response.body().getProductbyid();

                int varIdProduct = listData.getProductid();
                String varNamaProduct = listData.getName();
                int varHargaProduct = listData.getPrice();
                int varCatProduct = listData.getCat_id();
                int varStock =listData.getStock();
                String varShortDesk = listData.getShort_desc();
                String varBrand = listData.getBrand();
                String varImg = listData.getImg();
                String varDesc = listData.getDesc();


                EditProductFragment epf = new EditProductFragment();
                Bundle args = new Bundle();
                args.putInt("id", varIdProduct);
                args.putInt("price", varHargaProduct);
                args.putInt("stock", varStock);
                args.putInt("cat", varCatProduct);
                args.putString("name", varNamaProduct);
                args.putString("brand",varBrand);
                args.putString("short", varShortDesk);
                args.putString("desc", varDesc);
                args.putString("img", varImg);
                epf.setArguments(args);

                //Toast.makeText(getActivity(),varBrand, Toast.LENGTH_SHORT).show();

//                            listData = response.body().getProduct();
                replaceFragment(epf,"edit_product");
                //removeFragment();
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(getActivity(), "Gagal Menghubungi Server : " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void replaceFragment(Fragment fragment,String targettag){
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(((ViewGroup)getView().getParent()).getId(),fragment,targettag);
        fragmentTransaction.addToBackStack("details_product").commit();
    }
    void removeFragment(){
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.remove(this);
        fragmentTransaction.commit();
    }
}