package com.example.selabeautyadmin.ui.staff;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.selabeautyadmin.R;
import com.example.selabeautyadmin.SPHandler;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class StaffAdapter extends RecyclerView.Adapter<StaffAdapter.HolderData>{
    private Context ctx;
    private List<DashboardViewModel> listData;
    private DashboardViewModel listProductData;
    private DashboardFragment frag;
    private int pId;
    InputStream is;
    Bitmap bmImg = null;
    SPHandler sp = new SPHandler();
    EventListener listener;
    String pd;

    List<Integer> in = new ArrayList<>();

    public List<Integer> addNo (List<Integer> i, int n){
        int a;
        for(a=1; a<n; ++a){
            i.add(a);
        }
        return i;
    }


    public interface EventListener {
        void onEvent(int data);
    }
    public StaffAdapter(Context ctx, List<DashboardViewModel> listData, EventListener l) {
        this.listener = l;
        this.ctx = ctx;
        this.listData = listData;
    }

    @NonNull
    @Override
    public HolderData onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.staff, parent, false);
        HolderData holder = new HolderData(layout);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull HolderData holder, int position) {
        DashboardViewModel pm = listData.get(position);
        List<Integer> iter = addNo(in, getItemCount()+1);
        holder.id.setText(String.valueOf(pm.getUserid()));
        holder.Nama.setText(pm.getName());
        holder.Email.setText(String.valueOf(pm.getAge()));
        holder.No.setText(String.valueOf(iter.get(position)));    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class HolderData extends RecyclerView.ViewHolder {
        TextView id, Username, Nama, Email, No, Age;


        public HolderData(@NonNull View itemView) {
            super(itemView);

            id = itemView.findViewById(R.id.staffid);
            No = itemView.findViewById(R.id.no);
            Nama = itemView.findViewById(R.id.nama);
            Email = itemView.findViewById(R.id.email);



            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onEvent(Integer.parseInt(id.getText().toString()));
                }
            });
//
        }
    }
}
