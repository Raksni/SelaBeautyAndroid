package com.example.selabeautyadmin;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIRequestData {

    @FormUrlEncoded
    @POST("login")
    Call<ResponseModel> ardLogin(
            @Field("email") String email,
            @Field("password") String password
    );

    @GET("admin/products")
    Call<ResponseModel> ardProductData(
            @Header("Authorization") String token,
            @Query("cat") int pageSize
    );

    @GET("admin/products/{id}")
    Call<ResponseModel> ardProductDataId(
            @Header("Authorization") String token,
            @Path("id") int id
    );


    @FormUrlEncoded
    @POST("admin/products")
    Call<ResponseModel> ardCreateProduct(
            @Header("Authorization") String token,
            @Field("name") String name,
            @Field("price") int price,
            @Field("stock") int stock,
            @Field("brand") String brand,
            @Field("categories") int categories,
            @Field("short_description") String short_desc,
            @Field("description") String desc,
            @Field("img") String img
    );



    @DELETE("admin/products/{id}")
    Call<ResponseModel> ardDeleteProduct(
            @Header("Authorization") String token,
            @Path("id") int id
    );

    @FormUrlEncoded
    @PUT("admin/products/{id}")
    Call<ResponseModel> ardUpdateProduct(
            @Header("Authorization") String token,
            @Path("id") int id,
            @Field("name") String name,
            @Field("price") int price,
            @Field("stock") int stock,
            @Field("brand") String brand,
            @Field("categories") int categories,
            @Field("short_description") String short_desc,
            @Field("description") String desc,
            @Field("img") String img
    );

    @GET("admin/users/{id}")
    Call<ResponseModel> ardStaffId(
            @Header("Authorization") String token,
            @Path("id") int id
    );

    @GET("admin/profile")
    Call<ResponseModel> ardProfile(
            @Header("Authorization") String token
    );

    @GET("admin/users")
    Call<ResponseModel> ardStaff(
            @Header("Authorization") String token,
            @Query("sort") int pos,
            @Query("ket") String ket
    );

    @FormUrlEncoded
    @POST("admin/users")
    Call<ResponseModel> ardAddStaff(
            @Header("Authorization") String token,
            @Field("name") String name,
            @Field("username") String uname,
            @Field("email") String email,
            @Field("password") String pass,
            @Field("age") int age
    );

    @FormUrlEncoded
    @PUT("admin/users/{id}")
    Call<ResponseModel> ardUpdateStaff(
            @Header("Authorization") String token,
            @Path("id") int id,
            @Field("name") String name,
            @Field("username") String uname,
            @Field("email") String email,
            @Field("age") int age
    );


    @FormUrlEncoded
    @PUT("admin/profile/{id}")
    Call<ResponseModel> ardUpdateProfile(
            @Header("Authorization") String token,
            @Path("id") int id,
            @Field("name") String name,
            @Field("username") String uname,
            @Field("email") String email,
            @Field("age") int age
    );

    @FormUrlEncoded
    @PUT("admin/users/change-password/{id}")
    Call<ResponseModel> ardChangePassStaff(
            @Header("Authorization") String token,
            @Path("id") int id,
            @Field("old") String old,
            @Field("new") String newpass,
            @Field("confirm") String confirm
    );

    @DELETE("admin/users/{id}")
    Call<ResponseModel> ardDeleteStaff(
            @Header("Authorization") String token,
            @Path("id") int id
    );

}
