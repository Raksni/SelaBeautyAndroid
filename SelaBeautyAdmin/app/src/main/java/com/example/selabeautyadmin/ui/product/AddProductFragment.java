package com.example.selabeautyadmin.ui.product;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.selabeautyadmin.APIRequestData;
import com.example.selabeautyadmin.R;
import com.example.selabeautyadmin.ResponseModel;
import com.example.selabeautyadmin.RetroServer;
import com.example.selabeautyadmin.SPHandler;
import com.example.selabeautyadmin.databinding.FragmentAddProductBinding;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddProductFragment extends Fragment implements AdapterView.OnItemSelectedListener{

    private FragmentAddProductBinding binding;
    SPHandler sp = new SPHandler();
    String[] category = { "Bodycare", "Haircare",
            "Makeup", "Skincare"};

    EditText tvname, tvshort,tvstock, tvprice,tvdesc, tvbrand,imageView;
    Spinner tvcat;
    Button edit,cancel;
    private int uprice,ustock,ucat;
    private String uname,ubrand,ushort,udesc,uimg;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentAddProductBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        tvname = binding.pName;
        tvcat = binding.pCat;
        tvshort = binding.pStortdesc;
        tvprice = binding.pHarga;
        tvstock = binding.pStock;
        tvdesc = binding.pDesc;
        imageView = binding.pImg;
        tvbrand =binding.pBrand;
        edit = binding.btnAdd;

        ArrayAdapter ad = new ArrayAdapter(getActivity(), R.layout.category_value, category);

        ad.setDropDownViewResource(
                R.layout.category_value);
        tvcat.setAdapter(ad);
        tvcat.setSelection(0);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ucat = tvcat.getSelectedItemPosition()+1;
                uname = tvname.getText().toString();
                ubrand = tvbrand.getText().toString();
                ushort= tvshort.getText().toString();
                udesc= tvdesc.getText().toString();
                uimg= imageView.getText().toString();

                if(uname.trim().equals("")){
                    tvname.setError("Nama Harus Diisi");
                }
                else if(tvprice.getText().toString().trim().equals("")){
                    tvprice.setError("Harga Harus Diisi");
                }else if(tvprice.getText().toString().trim().equals("")){
                    tvprice.setError("Harga Harus Diisi");
                }else if(tvstock.getText().toString().trim().equals("")){
                    tvstock.setError("Stok Harus Diisi");
                }else if(ubrand.trim().equals("")){
                    tvbrand.setError("Brand Harus Diisi");
                }else if(ushort.trim().equals("")){
                    tvshort.setError("Deskripsi Harus Diisi");
                }else if(udesc.trim().equals("")){
                    tvdesc.setError("Deskripsi Harus Diisi");
                }else if(uimg.trim().equals("")){
                    imageView.setError("URL Gambar Harus Diisi");
                }else {
                    uprice =Integer.parseInt(tvprice.getText().toString());
                    ustock = Integer.parseInt(tvstock.getText().toString());
                    updateData();
                }
            }
        });
        return root;
    }
    private void updateData(){
        APIRequestData ardData = RetroServer.konekRetrofit().create(APIRequestData.class);
        Call<ResponseModel> ubahData = ardData.ardCreateProduct(sp.getToken(getActivity()),uname, uprice, ustock, ubrand, ucat, ushort, udesc, uimg);

        ubahData.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                String pesan = response.body().getMessage();
                Toast.makeText(getActivity(), "Produk berhasil ditambahkan", Toast.LENGTH_SHORT).show();
                getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                removeFragment();
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(getActivity(), "Gagal Menghubungi Server | "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
        ucat = tvcat.getSelectedItemPosition()+1;
    }
    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
    }

    void removeFragment(){
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.remove(this);
        fragmentTransaction.commit();
    }

}