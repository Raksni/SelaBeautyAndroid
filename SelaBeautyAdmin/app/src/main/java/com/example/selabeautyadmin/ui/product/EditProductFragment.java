package com.example.selabeautyadmin.ui.product;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.selabeautyadmin.APIRequestData;
import com.example.selabeautyadmin.R;
import com.example.selabeautyadmin.ResponseModel;
import com.example.selabeautyadmin.RetroServer;
import com.example.selabeautyadmin.SPHandler;
import com.example.selabeautyadmin.databinding.FragmentEditProductBinding;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EditProductFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditProductFragment extends Fragment implements AdapterView.OnItemSelectedListener{
    FragmentEditProductBinding binding;
    SPHandler sp = new SPHandler();
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String id = "id";
    private static final String price = "price";
    private static final String stock = "stock";
    private static final String cat = "cat";
    private static final String name = "name";
    private static final String shortdesc = "short";
    private static final String desc = "desc";
    private static final String img = "img";
    private static final String brand = "brand";

    // TODO: Rename and change types of parameters
    private int pid, uprice,ustock,ucat;
    private int pprice;
    private int pstock;
    private int pcat;
    private String pname,uname,ubrand,ushort,udesc,uimg;
    private String pbrand;
    private String pshort;
    private String pdesc;
    private String pimg;

    String[] category = { "Bodycare", "Haircare",
            "Makeup", "Skincare"};

    EditText tvname, tvshort,tvstock, tvprice,tvdesc, tvbrand,imageView;
    Spinner tvcat;
    Button edit,cancel;

    public EditProductFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *

     * @return A new instance of fragment ditProductFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EditProductFragment newInstance(int varIdProduct, int varHargaProduct, int varStock, int varCatProduct, String varNamaProduct, String varBrand, String varShortDesk, String varDesc, String varImg) {
        EditProductFragment fragment = new EditProductFragment();
        Bundle args = new Bundle();
        args.putInt("id", varIdProduct);
        args.putInt("price", varHargaProduct);
        args.putInt("stock", varStock);
        args.putInt("cat", varCatProduct);
        args.putString("name", varNamaProduct);
        args.putString("brand",varBrand);
        args.putString("short", varShortDesk);
        args.putString("desc", varDesc);
        args.putString("img", varImg);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            pid = getArguments().getInt(id);
            pprice = getArguments().getInt(price);
            pstock = getArguments().getInt(stock);
            pcat = getArguments().getInt(cat);
            pname = getArguments().getString(name);
            pbrand = getArguments().getString(brand);
            pshort = getArguments().getString(shortdesc);
            pdesc = getArguments().getString(desc);
            pimg = getArguments().getString(img);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentEditProductBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        tvname = binding.pName;
        tvcat = binding.pCat;
        tvshort = binding.pStortdesc;
        tvprice = binding.pHarga;
        tvstock = binding.pStock;
        tvdesc = binding.pDesc;
        imageView = binding.pImg;
        tvbrand =binding.pBrand;
        edit = binding.btnEdit;

        tvname.setText(pname);
        tvshort.setText(pshort);
        tvprice.setText(String.valueOf(pprice));
        tvstock.setText(String.valueOf(pstock));
        tvdesc.setText(pdesc);
        imageView.setText(pimg);
        tvbrand.setText(pbrand);
        tvcat.setOnItemSelectedListener(this);

        ArrayAdapter ad = new ArrayAdapter(getActivity(),R.layout.category_value, category);

        ad.setDropDownViewResource(
                R.layout.category_value);
        tvcat.setAdapter(ad);
        int p = pcat-1;
        tvcat.setSelection(p);

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ucat = tvcat.getSelectedItemPosition()+1;
                uname = tvname.getText().toString();
                uprice =Integer.parseInt(tvprice.getText().toString());
                ustock = Integer.parseInt(tvstock.getText().toString());
                ubrand = tvbrand.getText().toString();
                ushort= tvshort.getText().toString();
                udesc= tvdesc.getText().toString();
                uimg= imageView.getText().toString();

                if(uname.trim().equals("")){
                    tvname.setError("Nama Harus Diisi");
                }
                else if(tvprice.getText().toString().trim().equals("")){
                    tvprice.setError("Harga Harus Diisi");
                }else if(tvprice.getText().toString().trim().equals("")){
                    tvprice.setError("Harga Harus Diisi");
                }else if(tvstock.getText().toString().trim().equals("")){
                    tvstock.setError("Stok Harus Diisi");
                }else if(ubrand.trim().equals("")){
                    tvbrand.setError("Brand Harus Diisi");
                }else if(ushort.trim().equals("")){
                    tvshort.setError("Deskripsi Harus Diisi");
                }else if(udesc.trim().equals("")){
                    tvdesc.setError("Deskripsi Harus Diisi");
                }else if(uimg.trim().equals("")){
                    imageView.setError("URL Gambar Harus Diisi");
                }else {
                    updateData();
                }
            }
        });
        return root;
    }
    private void updateData(){
        APIRequestData ardData = RetroServer.konekRetrofit().create(APIRequestData.class);
        Call<ResponseModel> ubahData = ardData.ardUpdateProduct(sp.getToken(getActivity()),pid,uname, uprice, ustock, ubrand, ucat, ushort, udesc, uimg);

        ubahData.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                String pesan = response.body().getMessage();
                Toast.makeText(getActivity(), "Update berhasil", Toast.LENGTH_SHORT).show();
                getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                removeFragment();
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(getActivity(), "Gagal Menghubungi Server | "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
        ucat = tvcat.getSelectedItemPosition()+1;
    }
    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
    }

    void removeFragment(){
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.remove(this);
        fragmentTransaction.commit();
    }
}