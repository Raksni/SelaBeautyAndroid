package com.example.selabeautyadmin;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    Button login;
    TextView username, userpass;
    String user, pass,role,token,message;
            int error;
    ProgressDialog pbData;
    String adm = "admin";
    String staff = "staff";
    SPHandler sp = new SPHandler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        login = (Button) findViewById(R.id.admlogin);
        username = (TextView) findViewById(R.id.admuser);
        userpass = (TextView) findViewById(R.id.admpass);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                user = username.getText().toString();
                pass = userpass.getText().toString();

                if(user.trim().equals("")){ username.setError("Email Harus Diisi");}
                else if(pass.trim().equals("")){ userpass.setError("Password Harus Diisi");}
                else{ loginUser();}
            }});
    }

    private void loginUser(){
        pbData = new ProgressDialog(LoginActivity.this);
        pbData.setMessage("Logging In...");
        APIRequestData ardData = RetroServer.konekRetrofit().create(APIRequestData.class);
        Call<ResponseModel> login = ardData.ardLogin(user,pass);
        login.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                if(response.isSuccessful()){
                    role = response.body().getRole();
                    token = response.body().getToken();
                    sp.setToken(LoginActivity.this, token);
                    sp.setRole(LoginActivity.this, role);
                    Intent main = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(main);
                }else{
                    message ="Logging in Failed \n\n Wrong Email or Password";
                    Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                }
                pbData.hide();
            }
            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                pbData.hide();
            }
        });

    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this)
                .setTitle("Alert")
                .setCancelable(false)
                .setMessage("Are your sure want to exit?")
                .setIcon(R.drawable.sela_beauty_logo)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.finishAffinity(LoginActivity.this);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

        builder.show();

    }
}