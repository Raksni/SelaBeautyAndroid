package com.example.selabeautyadmin.ui.staff;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.selabeautyadmin.APIRequestData;
import com.example.selabeautyadmin.R;
import com.example.selabeautyadmin.ResponseModel;
import com.example.selabeautyadmin.RetroServer;
import com.example.selabeautyadmin.SPHandler;
import com.example.selabeautyadmin.databinding.FragmentDetailsStaffBinding;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DetailsStaffFragment#newInstance} factory method to
 * create an instance of this fragment.
 */

public class DetailsStaffFragment extends Fragment {
    SPHandler sp = new SPHandler();
    FragmentDetailsStaffBinding binding;
    private DashboardViewModel listData;
    ProgressDialog p;
    ExecutorService executor = Executors.newSingleThreadExecutor();
    Handler handler = new Handler(Looper.getMainLooper());

    private static final String id = "id";
    private static final String uname = "uname";
    private static final String email = "email";
    private static final String age = "age";
    private static final String name = "name";
    private static final String create = "create";

    // TODO: Rename and change types of parameters
    private int pid;
    private int page;
    private String pname;
    private String puname;
    private String pemail, pcreate;

    ImageView imageView;
    TextView tvid, tvname, tvuname, tvemail,tvage, tvcreate;
    Button btnedit, btndelete;

    // TODO: Rename and change types and number of parameters
    public static DetailsStaffFragment newInstance(int varIdProduct, int varHargaProduct,String varNamaProduct, String varBrand, String varShortDesk) {
        DetailsStaffFragment fragment = new DetailsStaffFragment();
        Bundle args = new Bundle();
        args.putInt("id", varIdProduct);
        args.putInt("age", varHargaProduct);
        args.putString("name", varNamaProduct);
        args.putString("email",varBrand);
        args.putString("uname", varShortDesk);
        return fragment;
    }
    public DetailsStaffFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment DetailsStaffFragment.
     */


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            pid = getArguments().getInt(id);
            page = getArguments().getInt(age);
            pname = getArguments().getString(name);
            pemail = getArguments().getString(email);
            puname = getArguments().getString(uname);
            pcreate = getArguments().getString(create);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentDetailsStaffBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        tvname = binding.name;
        tvuname = binding.uname;
        tvemail = binding.email;
        tvage = binding.age;
        btndelete = binding.delete;
        tvcreate = binding.create;
        btnedit = binding.edit;

        tvname.setText(pname);
        tvuname.setText(puname);
        tvemail.setText(pemail);
        tvage.setText(String.valueOf(page));
        tvcreate.setText("join at "+pcreate);

        btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder dialogPesan = new AlertDialog.Builder(getActivity());
                dialogPesan.setMessage("are you sure want to delete this staff?");
                dialogPesan.setTitle("Alert");
                dialogPesan.setIcon(R.drawable.sela_beauty_logo);
                dialogPesan.setCancelable(true);
                dialogPesan.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        deleteData(pid);
                        dialogInterface.dismiss();
                        Handler hand = new Handler();
                        hand.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                removeFragment();
                            }
                        }, 1000);
                    }
                });

                dialogPesan.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                dialogPesan.show();
            }
        });

        btnedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData(pid);
            }
        });

        return root;
    }

    private void deleteData(int p){
        APIRequestData ardData = RetroServer.konekRetrofit().create(APIRequestData.class);
        Call<ResponseModel> hapusData = ardData.ardDeleteStaff(sp.getToken(getActivity()), p);

        hapusData.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                String pesan = response.body().getMessage();

                Toast.makeText(getActivity(), "Pesan : "+pesan, Toast.LENGTH_SHORT).show();
                removeFragment();
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(getActivity(), "Gagal Menghubungi Server : " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getData(int p){
        APIRequestData ardData = RetroServer.konekRetrofit().create(APIRequestData.class);
        Call<ResponseModel> getProduct = ardData.ardStaffId(sp.getToken(getActivity()), p);

        getProduct.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                listData = response.body().getUserbyid();

                int varId = listData.getUserid();
                int varAge = listData.getAge();
                String varNama = listData.getName();
                String varEmail = listData.getEmail();
                String varUsername = listData.getUsername();
                String varCreate = listData.getCreate_at();

                EditStaffFragment esf = new EditStaffFragment();
                Bundle args = new Bundle();
                args.putInt("id", varId);
                args.putInt("age", varAge);
                args.putString("name", varNama);
                args.putString("email",varEmail);
                args.putString("uname", varUsername);
                args.putString("create",varCreate);
                esf.setArguments(args);
                replaceFragment(esf,"update_staff");
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(getActivity(), "Gagal Menghubungi Server : " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void replaceFragment(Fragment fragment,String targettag){
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(((ViewGroup)getView().getParent()).getId(),fragment,targettag);
        fragmentTransaction.addToBackStack("staff_detail").commit();
    }

    void removeFragment(){
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.remove(this);
        fragmentTransaction.commit();
    }
}