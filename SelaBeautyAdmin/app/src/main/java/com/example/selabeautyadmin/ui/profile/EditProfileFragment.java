package com.example.selabeautyadmin.ui.profile;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.selabeautyadmin.APIRequestData;
import com.example.selabeautyadmin.ResponseModel;
import com.example.selabeautyadmin.RetroServer;
import com.example.selabeautyadmin.SPHandler;
import com.example.selabeautyadmin.databinding.FragmentEditProfileBinding;
import com.example.selabeautyadmin.ui.staff.DashboardViewModel;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EditProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditProfileFragment extends Fragment {

    SPHandler sp = new SPHandler();
    FragmentEditProfileBinding binding;
    private DashboardViewModel listData;
    ProgressDialog p;
    ExecutorService executor = Executors.newSingleThreadExecutor();
    Handler handler = new Handler(Looper.getMainLooper());

    private static final String id = "id";
    private static final String uname = "uname";
    private static final String email = "email";
    private static final String age = "age";
    private static final String name = "name";
    private static final String create = "create";

    // TODO: Rename and change types of parameters
    private int pid, uid;
    private int page, uage;
    private String pname,urname;
    private String puname, uuname;
    private String pemail, uemail, pcreate;

    TextView tvid, tvname, tvuname, tvemail,tvage,tvcreate;
    Button btnedit;

    // TODO: Rename and change types and number of parameters
    public static EditProfileFragment newInstance(int varIdProduct, int varHargaProduct, String varNamaProduct, String varBrand, String varShortDesk) {
        EditProfileFragment fragment = new EditProfileFragment();
        Bundle args = new Bundle();
        args.putInt("id", varIdProduct);
        args.putInt("age", varHargaProduct);
        args.putString("name", varNamaProduct);
        args.putString("email",varBrand);
        args.putString("uname", varShortDesk);
        return fragment;
    }
    public EditProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment EditProfileFragment.
     */


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            pid = getArguments().getInt(id);
            page = getArguments().getInt(age);
            pname = getArguments().getString(name);
            pemail = getArguments().getString(email);
            puname = getArguments().getString(uname);
            pcreate = getArguments().getString(create);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentEditProfileBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        tvid = binding.staffid;
        tvname = binding.name;
        tvuname = binding.username;
        tvemail = binding.email;
        tvage = binding.age;
        btnedit = binding.btnEdit;

        tvid.setText(String.valueOf(pid));
        tvname.setText(pname);
        tvuname.setText(puname);
        tvemail.setText(pemail);
        tvage.setText(String.valueOf(page));


        btnedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uid = Integer.parseInt(tvid.getText().toString());
                uage = Integer.parseInt(tvage.getText().toString());
                uuname = tvuname.getText().toString();
                uemail = tvemail.getText().toString();
                urname = tvname.getText().toString();

                if(urname.trim().equals("")){
                    tvname.setError("Nama Harus Diisi");
                } else if(uuname.trim().equals("")){
                    tvuname.setError("Username Harus Diisi");
                } else if(uemail.trim().equals("")){
                    tvemail.setError("Email Harus Diisi");
                } else if(tvage.getText().toString().trim().equals("")){
                    tvage.setError("Umur Harus Diisi");
                } else {
                    update();
                    //Toast.makeText(getActivity(), uid+"| "+urname+"| "+uuname+"| "+uemail+"| "+uage, Toast.LENGTH_SHORT).show();
                }
            }
        });

        return root;
    }

    private void update(){
        APIRequestData ardData = RetroServer.konekRetrofit().create(APIRequestData.class);
        Call<ResponseModel> hapusData = ardData.ardUpdateProfile(sp.getToken(getActivity()), uid, urname,uuname, uemail, uage);

        hapusData.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                //Toast.makeText(getActivity(), sp.getToken(getActivity())+"| "+uid+"| "+urname+"| "+uuname+"| "+uemail+"| "+uage, Toast.LENGTH_SHORT).show();
                if(response.isSuccessful()) {
                    String pesan = response.body().getMessage();
                    String token =response.body().getToken();
                    String role = response.body().getRole();
                    sp.setToken(getActivity(),token);
                    sp.setRole(getActivity(), role);
                    Toast.makeText(getActivity(), "Berhasil mengubah profil " + pname, Toast.LENGTH_SHORT).show();
                    getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    removeFragment();
                }else{
                    //ring pesan = response.body().getMessage();
                    Toast.makeText(getActivity(), "Gagal mengubah data staff ", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(getActivity(), "Gagal Menghubungi Server : " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    void removeFragment(){
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.remove(this);
        fragmentTransaction.commit();
    }
}