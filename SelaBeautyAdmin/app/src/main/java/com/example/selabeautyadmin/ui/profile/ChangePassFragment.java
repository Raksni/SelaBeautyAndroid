package com.example.selabeautyadmin.ui.profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.selabeautyadmin.APIRequestData;
import com.example.selabeautyadmin.ResponseModel;
import com.example.selabeautyadmin.RetroServer;
import com.example.selabeautyadmin.SPHandler;
import com.example.selabeautyadmin.databinding.FragmentChangePassBinding;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ChangePassFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChangePassFragment extends Fragment {

    FragmentChangePassBinding binding;
    SPHandler sp = new SPHandler();

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "id";

    // TODO: Rename and change types of parameters
    private int uid;
    private String old, newp, conf;
    EditText pold, pnew, pconf;
    Button edit;

    public ChangePassFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param id Parameter 1.
     * @return A new instance of fragment ChangePassFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChangePassFragment newInstance(int id) {
        ChangePassFragment fragment = new ChangePassFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            uid = getArguments().getInt(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentChangePassBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        pold = binding.passOld;
        pnew = binding.passNew;
        pconf = binding.passConf;
        edit = binding.btnEdit;

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                old = pold.getText().toString();
                newp = pnew.getText().toString();
                conf = pconf.getText().toString();

                if(old.trim().equals("")){
                    pold.setError("Password Lama Harus Diisi");
                } else if(newp.trim().equals("")){
                    pold.setError("Password Baru Harus Diisi");
                }
                else if(conf.trim().equals("")){
                    pconf.setError("Konfirmasi Password Harus Diisi");
                } else if(!newp.equals(conf)){
                    pconf.setError("Password Tidak Sama");
                } else{
                    changePass();
                }
            }
        });
        return root;
    }

    private void changePass(){
        APIRequestData ardData = RetroServer.konekRetrofit().create(APIRequestData.class);
        Call<ResponseModel> ganti = ardData.ardChangePassStaff(sp.getToken(getActivity()),uid, old, newp, conf);

        ganti.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                if(response.isSuccessful()) {
                    //String pesan = response.body().getMessage();

                    Toast.makeText(getActivity(), "Berhasil mengubah password", Toast.LENGTH_SHORT).show();
                    getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    removeFragment();
                }
                else{
                    Toast.makeText(getActivity(), "Ggagal mengubah password\n Password salah", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(getActivity(), "Gagal Menghubungi Server : " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    void removeFragment(){
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.remove(this);
        fragmentTransaction.commit();
    }
}