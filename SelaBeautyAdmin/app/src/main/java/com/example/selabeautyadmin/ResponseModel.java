package com.example.selabeautyadmin;

import com.example.selabeautyadmin.ui.product.CategoryModel;
import com.example.selabeautyadmin.ui.product.HomeViewModel;
import com.example.selabeautyadmin.ui.staff.DashboardViewModel;

import java.util.List;
public class ResponseModel {
        private String message,token,role,uid,status,name,productid;
        private int error;
        private HomeViewModel productbyid;
        private DashboardViewModel userbyid;
        private List<HomeViewModel> product;
        private List<DashboardViewModel> user;
        private List<CategoryModel> category;

        public String getProductid() {
                return productid;
        }

        public void setProductid(String productid) {
                this.productid = productid;
        }

        public int getError() {
                return error;
        }

        public void setError(int error) {
                this.error = error;
        }

        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }

        public String getStatus() {
                return status;
        }

        public void setStatus(String status) {
                this.status = status;
        }

        public String getMessage() {
                return message;
        }

        public void setMessage(String message) {
                this.message = message;
        }

        public String getToken() {
                return token;
        }

        public void setToken(String token) {
                this.token = token;
        }

        public List<HomeViewModel> getProduct() {
                return product;
        }

        public void setProduct(List<HomeViewModel> product) {
                this.product = product;
        }

        public List<CategoryModel> getCategory() {
                return category;
        }

        public void setCategory(List<CategoryModel> category) {
                this.category = category;
        }

        public String getRole() {
                return role;
        }

        public void setRole(String role) {
                this.role = role;
        }

        public String getUid() {
                return uid;
        }

        public void setUid(String uid) {
                this.uid = uid;
        }

        public HomeViewModel getProductbyid() {
                return productbyid;
        }

        public void setProductbyid(HomeViewModel productbyid) {
                this.productbyid = productbyid;
        }

        public DashboardViewModel getUserbyid() {
                return userbyid;
        }

        public List<DashboardViewModel> getUser() {
                return user;
        }
}
