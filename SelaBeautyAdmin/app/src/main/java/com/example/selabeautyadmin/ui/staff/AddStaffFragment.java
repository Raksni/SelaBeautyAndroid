package com.example.selabeautyadmin.ui.staff;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.selabeautyadmin.APIRequestData;
import com.example.selabeautyadmin.ResponseModel;
import com.example.selabeautyadmin.RetroServer;
import com.example.selabeautyadmin.SPHandler;
import com.example.selabeautyadmin.databinding.FragmentAddStaffBinding;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddStaffFragment extends Fragment {
    private FragmentAddStaffBinding binding;
    SPHandler sp = new SPHandler();
    private int page;
    private String pname;
    private String puname;
    private String pemail,ppass,pconf;

    TextView tvname, tvuname, tvemail,tvage, tvpass, tvconf;
    Button btnedit;

    public AddStaffFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentAddStaffBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        tvname = binding.name;
        tvuname = binding.username;
        tvemail = binding.email;
        tvage = binding.age;
        tvpass = binding.pass;
        tvconf = binding.confpass;
        btnedit = binding.btnAdd;

        btnedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(tvname.getText().toString().equals("")){
                    tvname.setError("Nama Harus Diisi");
                } else if(tvuname.getText().toString().equals("")){
                    tvuname.setError("Username Harus Diisi");
                } else if(tvemail.getText().toString().equals("")){
                    tvemail.setError("Email Harus Diisi");
                } else if(tvage.getText().toString().trim().equals("")){
                    tvage.setError("Umur Harus Diisi");
                } else if(tvpass.getText().toString().equals("")){
                    tvpass.setError("Password Harus Diisi");
                } else if(tvconf.getText().toString().equals("")){
                    tvconf.setError("Konfirmasi Password Harus Diisi");
                } else if(!tvpass.getText().toString().equals(tvconf.getText().toString())){
                    tvconf.setError("Password Tidak Sama");
                } else{
                    page = Integer.parseInt(tvage.getText().toString());
                    pname = tvname.getText().toString();
                    puname = tvuname.getText().toString();
                    pemail = tvemail.getText().toString();
                    ppass = tvpass.getText().toString();
                    pconf = tvconf.getText().toString();
                    addData();
                }

            }
        });
        return root;
    }
    public void addData(){
        APIRequestData ardData = RetroServer.konekRetrofit().create(APIRequestData.class);
        Call<ResponseModel> addData = ardData.ardAddStaff(sp.getToken(getActivity()),pname,puname, pemail,ppass, page);

        addData.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                String pesan = response.body().getMessage();
                Toast.makeText(getActivity(), "Pesan : "+pesan, Toast.LENGTH_SHORT).show();
                removeFragment();
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(getActivity(), "Gagal Menghubungi Server : " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    void removeFragment(){
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.remove(this);
        fragmentTransaction.commit();
    }
}