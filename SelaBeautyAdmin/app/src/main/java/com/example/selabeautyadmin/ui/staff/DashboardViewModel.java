package com.example.selabeautyadmin.ui.staff;

import androidx.lifecycle.ViewModel;

public class DashboardViewModel extends ViewModel {

    private String username, name, email, create_at;
    private int userid,age;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getUserid() {
        return userid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCreate_at() {
        return create_at;
    }

}