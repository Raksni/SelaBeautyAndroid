package com.example.selabeautyadmin.ui.product;

import androidx.lifecycle.ViewModel;

public class HomeViewModel extends ViewModel {
    private int productid, price, stock, category_id;
    private String name, brand, short_description, description, img;


    public String getName() {
        return name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShort_desc() {
        return short_description;
    }

    public void setShort_desc(String short_desc) {
        this.short_description = short_desc;
    }

    public String getDesc() {
        return description;
    }

    public void setDesc(String desc) {
        this.description = desc;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getProductid() {
        return productid;
    }

    public void setProductid(int productid) {
        this.productid = productid;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getCat_id() {
        return category_id;
    }

    public void setCat_id(int cat_id) {
        this.category_id = cat_id;
    }
}