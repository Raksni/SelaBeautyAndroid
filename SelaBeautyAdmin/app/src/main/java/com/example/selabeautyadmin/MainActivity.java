package com.example.selabeautyadmin;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    SPHandler sp = new SPHandler();
    String adm = "admin";
    String staff = "staff";
    Timer timer = new Timer();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTime();
    }
    void setTime() {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (sp.getToken(MainActivity.this) == "") {
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                } else {
                    if (sp.getRole(MainActivity.this).equals(adm)) {
                        Intent adm = new Intent(MainActivity.this, NavActivity.class);
                        startActivity(adm);
                    } else if (sp.getRole(MainActivity.this).equals(staff)) {
                        Intent staff = new Intent(MainActivity.this, StaffNavctivity.class);
                        startActivity(staff);
                    }
                }
            }

        }, 2000);

    }
    public void onResume() {
        super.onResume();
        setTime();
    }
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this)
                .setTitle("Alert")
                .setCancelable(false)
                .setMessage("Are your sure want to exit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.finishAffinity(MainActivity.this);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

        builder.show();
    }
}