package com.example.selabeautyadmin.ui.staff;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.selabeautyadmin.APIRequestData;
import com.example.selabeautyadmin.R;
import com.example.selabeautyadmin.ResponseModel;
import com.example.selabeautyadmin.RetroServer;
import com.example.selabeautyadmin.SPHandler;
import com.example.selabeautyadmin.databinding.FragmentDashboardBinding;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardFragment extends Fragment implements StaffAdapter.EventListener{
    SPHandler sp = new SPHandler();
    private RecyclerView review;
    private SwipeRefreshLayout srlData;
    private List<DashboardViewModel> listData = new ArrayList<>();
    private DashboardViewModel listProductData;
    private ProgressBar pbData;
    private FloatingActionButton fabTambah, fabfilter;
    private RecyclerView.LayoutManager lmData;
    private RecyclerView.Adapter adapter;
    private ToggleButton tg;
    private String sort;
    Spinner spin;
    TextView tv;
    String[] category = { "None","Name", "Username",
            "Age", "Date Joined"};
    private FragmentDashboardBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        DashboardViewModel dashboardViewModel =
                new ViewModelProvider(this).get(DashboardViewModel.class);
        binding = FragmentDashboardBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        review = binding.rvData;
        srlData = binding.srlData;
        pbData = binding.pbData;
        fabTambah = binding.addicon;
        tg = binding.toggle;
        lmData = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        review.setLayoutManager(lmData);
        spin = binding.pCat;
        tv = binding.rv;
        fabfilter = binding.filtericon;
        spin.setVisibility(View.INVISIBLE);
        ArrayAdapter ad = new ArrayAdapter(getActivity(), R.layout.category_value, category);
        ad.setDropDownViewResource(
                R.layout.category_value);
        spin.setAdapter(ad);
        spin.setSelection(0);
        retrieveData();

        srlData.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                srlData.setRefreshing(true);
                retrieveData();
                srlData.setRefreshing(false);
            }
        });
        tg.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    sort = "ASC";
                    retrieveData();
                } else {
                    // The toggle is disabled
                    sort = "DESC";
                    retrieveData();
                }
            }
        });
        fabTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddStaffFragment nextFrag= new AddStaffFragment();
                replaceFragment(nextFrag,"add_staff");
                fabTambah.setVisibility(View.INVISIBLE);
            }
        });
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                retrieveData();
            }
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });


        fabfilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv.setVisibility(View.VISIBLE);
                tg.setVisibility(View.VISIBLE);
                spin.setVisibility(View.VISIBLE);
            }
        });

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        retrieveData();
    }

    public void retrieveData(){
        pbData.setVisibility(View.VISIBLE);
        APIRequestData ardData = RetroServer.konekRetrofit().create(APIRequestData.class);
        Call<ResponseModel> showProduct = ardData.ardStaff(sp.getToken(getActivity()),spin.getSelectedItemPosition(),sort);
        showProduct.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                if(response.isSuccessful()) {
                    listData = response.body().getUser();
                    adapter = new StaffAdapter(getActivity(), listData, DashboardFragment.this);
                    review.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
                tv.setVisibility(View.INVISIBLE);
                tg.setVisibility(View.INVISIBLE);
                spin.setVisibility(View.INVISIBLE);
                pbData.setVisibility(View.INVISIBLE);
//
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(getActivity(), "Pesan"+t.getMessage(), Toast.LENGTH_SHORT).show();
                tv.setVisibility(View.VISIBLE);
                tg.setVisibility(View.VISIBLE);
                spin.setVisibility(View.VISIBLE);
                pbData.setVisibility(View.INVISIBLE);
            }
        });


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }



    @Override
    public void onEvent(int  d) {
        getData(d);
    }

    private void getData(int p){
        APIRequestData ardData = RetroServer.konekRetrofit().create(APIRequestData.class);
        Call<ResponseModel> getStaff = ardData.ardStaffId(sp.getToken(getActivity()), p);

        getStaff.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                listProductData = response.body().getUserbyid();
//
                int varId = listProductData.getUserid();
                int varAge = listProductData.getAge();
                String varNama = listProductData.getName();
                String varEmail = listProductData.getEmail();
                String varUsername = listProductData.getUsername();
                String varCreate = listProductData.getCreate_at();

                DetailsStaffFragment dsf = new DetailsStaffFragment();
                Bundle args = new Bundle();
                args.putInt("id", varId);
                args.putInt("age", varAge);
                args.putString("name", varNama);
                args.putString("email",varEmail);
                args.putString("uname", varUsername);
                args.putString("create",varCreate);
                dsf.setArguments(args);
                replaceFragment(dsf,"details_staff");
                fabTambah.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void replaceFragment(Fragment fragment,String targettag){
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(((ViewGroup)getView().getParent()).getId(),fragment,targettag);
        fragmentTransaction.addToBackStack("staff_fragment").commit();
    }

}