<?php namespace App\Models;

use CodeIgniter\Model;

class CategoryModel extends Model
{
    protected $table      = 'category';
	protected $primaryKey = 'id';

	protected $returnType     = 'array';

	protected $allowedFields = ['id','name'];

	protected $validationMessages = [];
	protected $skipValidation     = false;

	protected $beforeInsert = [];
}