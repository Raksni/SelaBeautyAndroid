<?php namespace App\Models;

use CodeIgniter\Model;

class ProductModel extends Model
{
    protected $table      = 'product';
	protected $primaryKey = 'productid';

	protected $returnType     = 'array';

	protected $allowedFields = [
		'productid',
		'brand',
		'name',
		'price',
		'stock',
		'category_id',
		'short_description',
		'description',
		'img',
	];

	protected $validationRules    = [
		'name'	=> 'required|min_length[3]',
		'price' => 'required',
		'brand' => 'required',
		'stock' => 'required',
		'category_id' => 'required',
		'short_description' => 'required',
		'description' => 'required',
		'img' => 'required',
	];

	protected $validationMessages = [];
	protected $skipValidation     = false;
}
