<?php

namespace App\Models;
use CodeIgniter\Model;
use Exception;
class UserModel extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'userid';
    protected $returnType = 'array';
    protected $allowedFields = [
        'userid',
        'username',
        'name',
        'age',
        'email',
        'password',
        'role',
        'create_at'];


    // Validation
    // protected $validationRules = [
    //     'username'=>'required',
    //     'name' => 'required',
    //     'email'=> 'required|valid_email',
    //     'password'=> 'required|min_length[6]'
    // ];

    function getEmail($email){
        $builder = $this->table("users");
        $data = $builder->where("email", $email)->first();
        if(!$data){
            throw new Exception("Data tidak ditemukan");
        }
        return $data;
    }
    protected $validationMessages = [];
	protected $skipValidation     = false;
}