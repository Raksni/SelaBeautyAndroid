<?php

namespace App\Controllers;
use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\UserModel;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class Me extends ResourceController
{
    /**
     * Return an array of resource objects, themselves in array format

    *
    * @return mixed
    */
    use ResponseTrait;
    protected $user;

    public function __construct(){
        $this->userModel = new UserModel();
    }
    public function index()
    {
        $key = getenv('TOKEN_SECRET');
        $authheader = $this->request->getHeader("Authorization");
        $authheader = $authheader->getValue();
        if(!$authheader) return Services::response()
            ->setJSON(['message' => 'Token Required'])
            ->setStatusCode(ResponseInterface::HTTP_UNAUTHORIZED);
        $token = $authheader;

        //$decoded = JWT::decode($token, new Key($key,'HS256')); 
        try {
            $decoded = JWT::decode($token, new Key($key,'HS256'));     
            $data = [
                    'userid' => $decoded->uid,
                    'name' => $decoded->name,
                    'username'=>$decoded->username,                
                    'email' => $decoded->email,
                    'age'=>$decoded->age,
                    'create_at'=>$decoded->create
                ];
                $this->user['userbyid'] = $data;
                return $this->respond($this->user);
            } catch (\Throwable $th) {
                return $this->fail('Invalid Token');
            }
    }

    public function edit_user($id){
        helper(['form']);
        $rules = [
            'username'=>'required',
            'name' => 'required',
            'age'=>'required',
            'email'=> 'required|valid_email'
        ];
        if(!$this->validate($rules)) return $this->fail($this->validator->getErrors());
        $json = $this->request->getJSON();
        if ($json) {
            $data = [
                'name' => $json->name,
                'email' =>$json->email,
                'age'=>$json->age,
                'username'=>$json->username
            ];
        } else {
            $input = $this->request->getRawInput();
            $data = [
                'name' => $input['name'],
                'email' =>$input['email'],
                 'age'=>$input['age'],
                'username'=>$input['username']
            ];
        }
        $update = $this->userModel->update($id, $data);
        $key = getenv('TOKEN_SECRET');
        $user= $this->userModel->where('userid',$id)->first();
        if($update){
            $payload = array(
                "iat" => 1356999524,
                "nbf" => 1357000000,
                "uid" => $id,
                "name"=>$data['name'],
                "username"=>$data['username'],
                "email" => $data['email'],
                "age"=>$data['age'],
                "create"=>$user['create_at']
            );
            $token = JWT::encode($payload, $key, 'HS256');
            $response = [
                'status' => 200,
                'error' => null,
                'message' => 'Data user berhasil diubah.',
                'token'=>$token,
                'role'=>$user['role']
                
            ];
        }else{
            $response = [
                'status' => 404,
                'error' => 404,
                'message' => 'Data user gagal diubah.'
            ];
        }
        
        return $this->respond($response);
    }

    
}