<?php namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\CategoryModel;

class Categories extends BaseController
{
    use ResponseTrait;
    protected $categoryModel;

    public function __construct()
    {
        $this->categoryModel = new CategoryModel();
    }

    public function index($categoryId = null)
    {
        if ($categoryId) {
            $category = $this->categoryModel->find($categoryId);
            if (!$category) {
                return $this->failNotFound('Category Not Found');
            }
            $this->data['category'] = $category;
        }else{
            $this->data['category'] = $this->categoryModel->findAll();
        }
        
        return $this->respond($this->data);
    }
    

}