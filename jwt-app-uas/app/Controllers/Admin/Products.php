<?php namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\ProductModel;
use App\Models\CategoryModel;

class Products extends BaseController
{
    use ResponseTrait;
    protected $productModel;

    public function __construct()
    {
        $this->productModel = new ProductModel();
        $this->categoryModel = new CategoryModel();
    }

    public function index($id=null)
    {
       if($id){
            $this->data['productbyid']= $this->productModel->where('productid',$id)->first();
            $this->data['message']='berhasil mengambil produk';
       }else{
        $cat = $this->request->getVar('cat');
        if($cat!=0){
            $this->data['product'] = $this->productModel->where('category_id', $cat)->findAll();            
            $this->data['error'] = null;
        }else{
            $this->data['product'] = $this->productModel->orderBy('name', 'ASC')->findAll();            
            $this->data['error'] = null;
        }
       }
        
        return $this->respond($this->data);
    }
    


    public function store()
    {
        $params = [
			'name' => $this->request->getVar('name'),			
            'price' => $this->request->getVar('price'),
			'stock' => $this->request->getVar('stock'),
			'brand' => $this->request->getVar('brand'),
			'category_id' => $this->request->getVar('categories'),
			'short_description' => $this->request->getVar('short_description'),
			'description' => $this->request->getVar('description'),
            'img'=>$this->request->getVar('img')
        ];

        $save = $this->productModel->insert($params);
        if($save){
            $response = [
                'status' => 201,
                'message' => 'produk berhasil ditambahkan.'
            ];
        }
        else{
            $response = [
                'error' => 404,
                'message' => 'produk gagal ditambahkan.'
            ];
        }
        return $this->respondCreated($response);
    }

    public function update($id)
    {
        $product = $this->productModel->find($id);
        if (!$product) {
            $response = [
                'error' => 404,
                'message' => 'produk tidak ditemukan.'
            ];
            return $this->respondCreated($response);
        }
        
        $json = $this->request->getJSON();
        if ($json) {
            $params = [
                'name' => $json->name,			
                'price' => $json->price,
                'stock' => $json->stock,
                'brand' => $json->brand,
                'category_id' => $json->categories,
                'short_description' => $json->short_description,
                'description' => $json->description,
                'img'=>$json->img
            ];
        } else {
            $input = $this->request->getRawInput();
            $params = [
                'name' => $input['name'],			
                'price' => $input['price'],
                'stock' => $input['stock'],
                'brand' => $input['brand'],
                'category_id' => $input['categories'],
                'short_description' => $input['short_description'],
                'description' => $input['description'],
                'img'=>$input['img']
            ];
        }
        

        
        
        $this->productModel->update($id,$params);
        $response = [
            'error' => null,
            'message' => 'Data produk berhasil diubah.'
        ];
        return $this->respond($response);
        
    }

    public function destroy($id)
    {
        $this->productModel->delete($id, true);

        $response = [
            'error' => null,
            'message' => 'produk berhasil dihapus.'
        ];
        return $this->respondCreated($response);
    }

}