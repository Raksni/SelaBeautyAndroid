<?php namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\UserModel;

class Users extends ResourceController
{
    use ResponseTrait;
    protected $userModel;

    public function __construct()
    {
        $this->userModel = new UserModel();
    }

    public function index($id = null)
    {
        $role = "staff";
        if($id){
            $data = $this->userModel->where('userid', $id)->first();
            if(!$data){
                return $this->failNotFound('Data tidak ditemukan.');
            }
            $this->data['userbyid'] = $data;
        }else{
            $sort = $this->request->getVar('sort');
            $ket = $this->request->getVar('ket');
            if($sort == 1){
                $this->data['user'] = $this->userModel->where('role','staff')->orderBy('name', $ket)->findAll();
            }else if($sort == 2){
                $this->data['user'] = $this->userModel->where('role','staff')->orderBy('username', $ket)->findAll();
            }else if($sort == 3){
                $this->data['user'] = $this->userModel->where('role','staff')->orderBy('age', $ket)->findAll();
            }else if($sort == 4){
                $this->data['user'] = $this->userModel->where('role','staff')->orderBy('create_at', $ket)->findAll();
            }else{
                $this->data['user'] = $this->userModel->where('role','staff')->findAll();
            }
            
            $this->data['error'] = null;
            
        }
        return $this->respond($this->data);
    }


    public function store()
    {
        helper(['form']);
        $rules = [
            'username'=>'required',
            'name' => 'required',
            'age'=>'required',
            'password'=> 'required|min_length[6]',
            'email' => 'required|valid_email'
        ];
        if(!$this->validate($rules)) return $this->fail($this->validator->getErrors());

        $params = [
			'name' => $this->request->getVar('name'),			
            'username' => $this->request->getVar('username'),
			'email' => $this->request->getVar('email'),
            'age' => $this->request->getVar('age'),
			'password' => password_hash($this->request->getVar('password'),PASSWORD_BCRYPT),			
        ];
        // $json = $this->request->getJSON();
        // if ($json) {
        //     $params = [
        //         'name' => $json->name,
        //         'email' =>$json->email,
        //         'username'=>$json->username,
        //         'password'=>password_hash($json->password, PASSWORD_BCRYPT)	
        //     ];
        // }else {
        //     $input = $this->request->getRawInput();
        //     $params = [                
        //         'name' => $input['name'],			
        //         'username' => $input['username'],
        //         'email' => $input['email'],
        //         'password' => password_hash($input['password'], PASSWORD_BCRYPT)			
        //     ];
        // }
        
        $insert = $this->userModel->insert($params);

        if($insert){
            $response = [
                'status' => 201,
                'message' => 'pengguna berhasil ditambahkan.'
            ];
        }
        else{
            $response = [
                'error' => 404,
                'message' => 'pengguna gagal ditambahkan.'
            ];
        }
        return $this->respondCreated($response);
    }
    public function edit_user($id){
        helper(['form']);
        $rules = [
            'username'=>'required',
            'name' => 'required',
            'age'=>'required',
            'email'=> 'required|valid_email'
        ];
        if(!$this->validate($rules)) return $this->fail($this->validator->getErrors());
        $json = $this->request->getJSON();
        if ($json) {
            $data = [
                'name' => $json->name,
                'email' =>$json->email,
                'age'=>$json->age,
                'username'=>$json->username
            ];
        } else {
            $input = $this->request->getRawInput();
            $data = [
                'name' => $input['name'],
                'email' =>$input['email'],
                 'age'=>$input['age'],
                'username'=>$input['username']
            ];
        }
        $update = $this->userModel->update($id, $data);
        if($update){
            $response = [
                'status' => 200,
                'error' => null,
                'message' => 'Data user berhasil diubah.'
                
            ];
        }else{
            $response = [
                'status' => 404,
                'error' => 404,
                'message' => 'Data user gagal diubah.'
            ];
        }
        
        return $this->respond($response);
    }

    public function change_password($id){
        

        helper(['form']);
        $rules = [
            'old'=>'required',
            'new' => 'required|min_length[6]',
            'confirm' => 'matches[new]'
        ];
        if(!$this->validate($rules)) return $this->fail($this->validator->getErrors());
        
        $user = $this->userModel->where('userid', $id)->first();
        $input = $this->request->getRawInput();
        $verify = password_verify($input['old'], $user['password']);

        if(!$verify) return $this->fail('Wrong Password');
        else{
            $data = [
                'password' => password_hash($input['new'], PASSWORD_BCRYPT),
            ];
            $update = $this->userModel->update($id,$data);
            if($update){
                $response = [
                    'error' => null,
                    'message' => 'berhasil mengubah password.'
                ];
            }else{
                $response = [
                    'error' => 404,
                    'message' => 'gagal mengubah passwordn.'
                ];
            }
            return $this->respond($response);
        }

    }

    public function destroy($id)
    {
        $this->userModel->delete($id, true);

        $response = [
            'error'=> null,
            'message' => 'staff berhasil dihapus.'
        ];
        return $this->respondCreated($response);
    }
}