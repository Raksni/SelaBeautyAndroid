<?php

namespace App\Controllers;
use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\UserModel;
use Firebase\JWT\JWT;


class Login extends ResourceController
{
    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    use ResponseTrait;
    // protected $data =[];
    // protected $session = null;
    // public function __construct(){
    //     $this->session = \Config\Services::session();
	// 	$this->data['session'] = $this->session;
    // }
    public function index()
    {
        helper(['form']);
        $rules = [
            'email' => 'required|valid_email',
            'password' => 'required|min_length[6]',
        ];
        $model = new UserModel();
        if(!$this->validate($rules)) return $this->fail($this->validator->getErrors());

        $user = $model->getEmail($this->request->getVar('email'));
        $verify = password_verify($this->request->getVar('password'), $user['password']);
        
        if(!$verify) return $this->fail('Wrong Password');
        $key = getenv('TOKEN_SECRET');
        $payload = array(
            "iat" => 1356999524,
            "nbf" => 1357000000,
            "uid" => $user['userid'],
            "name"=>$user['name'],
            "username"=>$user['username'],
            "email" => $user['email'],
            "age"=>$user['age'],
            "create"=>$user['create_at']
        );
        $token = JWT::encode($payload, $key, 'HS256');
        $data = [
            'error'=>0,
            'token'=>$token,
            'id'=>$user['userid'],
            'role' => $user['role']
        ];
        return $this->respond($data);
    }
}